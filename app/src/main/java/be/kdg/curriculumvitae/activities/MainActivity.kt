package be.kdg.curriculumvitae.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import be.kdg.curriculumvitae.R
import be.kdg.curriculumvitae.adapters.StudyAdapter

/**
 * Deze activity implementeert 'OnStudySelectedListener'!
 * Als er een study aangeklikt wordt dan zal deze klasse in actie moeten schieten.
 */
class MainActivity : AppCompatActivity(), StudyAdapter.OnStudySelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    /**
     * Hier reageren we op het feit dat een study aangeklikt wordt.
     */
    override fun onStudySelected(studyIndex: Int) {
        // TODO: aanvullen
    }
}
